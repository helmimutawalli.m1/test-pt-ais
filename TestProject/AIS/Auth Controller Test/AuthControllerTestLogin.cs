﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Response;
using Xunit;

namespace TestProject.AIS.Auth_Controller_Test
{
    public class AuthControllerTestLogin
    {
        [Fact]
        public void UserRead_Success()
        {
            var response = LoginTest(new LoginMessage
            {
                Email = "a@seeddata.com",
                Password = "s33dDataAdmin!"
            });

            var okResult = Assert.IsType<OkObjectResult>(response);
            var returnValue = Assert.IsType<LoginResponse>(okResult.Value);

            Assert.Equal("user-2", returnValue.UserId);
        }

        [Fact]
        public void UserRead_Failed()
        {
            var response = LoginTest(new LoginMessage
            {
                Email = "a@seeddata.com",
                Password = "s33dDataAdmin1"
            });

            var okResult = Assert.IsType<BadRequestObjectResult>(response);
            var returnValue = Assert.IsType<BasicResponse>(okResult.Value);

            Assert.Equal(ErrorUtil.InvalidLoginAttempt, returnValue.Error);
        }

        public IActionResult LoginTest(LoginMessage message)
        {
            var authController = AuthControllerTestHelper.MakeController();
            var response = authController.Login(message).Result;
            return response;
        }
    }
}
