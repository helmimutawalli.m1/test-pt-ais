﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using TestCaseBackendDeveloper_v._3.Configurations;
using TestCaseBackendDeveloper_v._3.Controllers;
using TestCaseBackendDeveloper_v._3.Services.Auth;
using TestCaseBackendDeveloper_v._3.Services.User;

namespace TestProject.AIS.Auth_Controller_Test
{
    public class AuthControllerTestHelper
    {
        public static AuthController MakeController()
        {
            var dbtest = new DatabaseContextTest();

            var entitiesUserData = DbInitializerUnitTest.InitializeUser(dbtest.Context);

            entitiesUserData.ForEach(t => t.State = EntityState.Detached);

            var userHandler = new UserHandler(dbtest.Context, new LoggerFactory());
            var authHandler = new AuthHandler(dbtest.Context, userHandler, Options.Create(new JWTOptions() { JWTSecret = "abcdefghijklmnoprstuvwxyz11234567890", TokenDuration = 10 }), new LoggerFactory());

            var authController = new AuthController(dbtest.Context, authHandler, new LoggerFactory());

            return authController;
        }
    }
}
