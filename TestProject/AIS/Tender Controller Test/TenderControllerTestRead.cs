﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Query;
using TestCaseBackendDeveloper_v._3.Query.General;
using TestCaseBackendDeveloper_v._3.Response;
using Xunit;
using System.Linq;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestCaseBackendDeveloper_v._3.Models;

namespace TestProject.AIS.Tender_Controller_Test
{
    public class TenderControllerTestRead
    {
        [Fact]
        public void TenderRead_Success()
        {
            var (response, entitiestender) = TenderReadTest(new TenderMessage
            {
                Query= new TenderQuery(),
                Order= new OrderQuery{
                    OrderBy= "ID",
                    IsDescending= true
                },
                Limit= new LimitQuery{
                    StartCount= 0,
                    EndCount= 5
                }
            }, "Admin");

            var okResult = Assert.IsType<OkObjectResult>(response);
            var returnValue = Assert.IsType<GenericResponse<IEnumerable<TenderDataForResponses>>>(okResult.Value);
            
            Assert.Equal(entitiestender.Count(), returnValue.Data.ToList().Count);
            returnValue.Data.ToList().ForEach(a => {
                var sameAdmin = entitiestender.ToList().Find(d => d.Entity.ID == a.ID);
                Assert.NotNull(sameAdmin);
                Assert.Equal(sameAdmin.Entity.Title, a.Title);
                Assert.Equal(sameAdmin.Entity.Description, a.Description);
                Assert.Equal(sameAdmin.Entity.Reference, a.Reference);
                Assert.Equal(sameAdmin.Entity.UserID, a.UserId);
                Assert.Equal(sameAdmin.Entity.ReleaseDate, a.ReleaseDate);
                Assert.Equal(sameAdmin.Entity.ClosingDate, a.ClosingDate);
            });
        }

        public (IActionResult, IEnumerable<EntityEntry<TenderModel>>) TenderReadTest(TenderMessage message, string role)
        {
            var (tenderController, entitiestender) = TenderControllerTestHelper.MakeController(role);
            var response = tenderController.Read(message).Result;
            return (response, entitiestender);
        }
    }
}
