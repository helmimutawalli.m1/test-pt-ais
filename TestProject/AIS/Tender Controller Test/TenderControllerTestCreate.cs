﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Response;
using Xunit;

namespace TestProject.AIS.Tender_Controller_Test
{
    public class TenderControllerTestCreate
    {
        [Fact]
        public void TenderCreate_Success()
        {
            var date = DateTime.Now;
            var (response, entitiestender) = TenderCreateTest(new TenderAddMessage
            {
                Title = "Test-1",
                Reference = "Reference-1",
                Description = "Description-1",
                ReleaseDate = date.AddMinutes(1),
                ClosingDate = date.AddMinutes(2)

            }, "Admin");

            var okResult = Assert.IsType<OkObjectResult>(response);
            var returnValue = Assert.IsType< GenericResponse<TenderDataForResponses>> (okResult.Value);

            Assert.Equal(2, returnValue.Data.UserId);
            Assert.Equal("Test-1", returnValue.Data.Title);
            Assert.Equal("Reference-1", returnValue.Data.Reference);
            Assert.Equal("Description-1", returnValue.Data.Description);
            Assert.Equal(date.AddMinutes(1), returnValue.Data.ReleaseDate);
            Assert.Equal(date.AddMinutes(2), returnValue.Data.ClosingDate);

        }

        public (IActionResult, IEnumerable<EntityEntry<TenderModel>>) TenderCreateTest(TenderAddMessage message, string role)
        {
            var (tenderController, entitiestender) = TenderControllerTestHelper.MakeController(role);
            var response = tenderController.Create(message).Result;
            return (response, entitiestender);
        }
    }
}
