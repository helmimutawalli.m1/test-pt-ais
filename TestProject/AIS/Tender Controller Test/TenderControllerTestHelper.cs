﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using TestCaseBackendDeveloper_v._3.Constants;
using TestCaseBackendDeveloper_v._3.Controllers;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Services.Tender;
using TestCaseBackendDeveloper_v._3.Services.User;

namespace TestProject.AIS.Tender_Controller_Test
{
    public class TenderControllerTestHelper
    {
        public static (TenderController, IEnumerable<EntityEntry<TenderModel>>) MakeController(string role = "")
        {
            var dbtest = new DatabaseContextTest();

            var entitiesUserData = DbInitializerUnitTest.InitializeUser(dbtest.Context);
            entitiesUserData.ForEach(t => t.State = EntityState.Detached);
            var entitiesTenderData = DbInitializerUnitTest.InitializeTender(dbtest.Context);
            entitiesTenderData.ForEach(t => t.State = EntityState.Detached);

            List<Claim> claims = new List<Claim>();
            if (role != "" && role != null)
            {
                claims.Add(
                    new Claim(ClaimTypes.NameIdentifier, entitiesUserData.Find(a => a.Entity.Role == role).Entity.UserId)
                );
                claims.Add(
                    new Claim(AisConstants.CLAIM_ROLE_KEY,
                        role == "Admin" ? AisConstants.CLAIM_ROLE_VALUE_ADMIN :
                        role == "User" ? AisConstants.CLAIM_ROLE_VALUE_USER :
                        ""
                )
                );
            }

            var mockHttpContext = new Mock<IHttpContextAccessor>();
            var httpContext = new DefaultHttpContext();
            httpContext.User.AddIdentity(new ClaimsIdentity(claims));
            mockHttpContext.Setup(t => t.HttpContext).Returns(httpContext);

            var userHandler = new UserHandler(dbtest.Context, new LoggerFactory());
            var tenderHandler = new TenderHandler(dbtest.Context, mockHttpContext.Object, userHandler, new LoggerFactory());

            var tenderController = new TenderController(dbtest.Context, mockHttpContext.Object, tenderHandler, new LoggerFactory());

            return (tenderController, entitiesTenderData);
        }
    }
}
