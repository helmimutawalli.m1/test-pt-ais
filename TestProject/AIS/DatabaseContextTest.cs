﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using TestCaseBackendDeveloper_v._3.Contexts;

namespace TestProject.AIS
{
    public class DatabaseContextTest
    {
        public readonly DatabaseContext Context;

        public DatabaseContextTest()
        {
            //Init database instance option to create instance in memory
            DbContextOptions<DatabaseContext> db_options = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                .EnableSensitiveDataLogging()
                .ConfigureWarnings(warn => warn.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            //Create DB context with previous options
            Context = new DatabaseContext(db_options);
        }
    }
}
