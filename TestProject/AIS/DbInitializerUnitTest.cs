﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Utils;

namespace TestProject.AIS
{
    public class DbInitializerUnitTest
    {
        public static List<EntityEntry<UserModel>> InitializeUser(DatabaseContext context)
        {
            List<EntityEntry<UserModel>> result = new List<EntityEntry<UserModel>>();

            if (!context.User.Any())
            {
                var date = DateTime.Now;

                var user1 = new UserModel
                {
                    ID = 1,
                    UserId = "user-1",
                    Email = "sa@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataSA!"),
                    Role = "Super Admin",
                    Username = "superadmin",
                    UpdateDate = date,
                    CreateDate = DateTime.Now,
                    Token = "",
                    LastRefresh = DateTime.Now.AddDays(-1)
                };

                var user2 = new UserModel
                {
                    ID = 2,
                    UserId = "user-2",
                    Email = "a@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataAdmin!"),
                    Role = "Admin",
                    Username = "admin",
                    CreateDate = date,
                    UpdateDate = date,
                    Token = "",
                    LastRefresh = DateTime.Now.AddDays(-1)
                };
                var user3 = new UserModel
                {
                    ID = 3,
                    UserId = "user-3",
                    Email = "u@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataUser!"),
                    Role = "User",
                    Username = "user",
                    CreateDate = date,
                    UpdateDate = date,
                    Token = "",
                    LastRefresh = DateTime.Now.AddDays(-1)
                };
                result.Add(context.User.Add(user1));
                result.Add(context.User.Add(user2));
                result.Add(context.User.Add(user3));


                context.SaveChanges();
            }

            return result;
        }

        public static List<EntityEntry<TenderModel>> InitializeTender(DatabaseContext context)
        {
            List<EntityEntry<TenderModel>> result = new List<EntityEntry<TenderModel>>();

            if (!context.Tender.Any())
            {
                var date = DateTime.Now;

                var tender1 = new TenderModel
                {
                    ID = 1,
                    UserID = 1,
                    Title = "title-1",
                    Reference = "ref-1",
                    ReleaseDate = date.AddMinutes(1),
                    ClosingDate = date.AddMinutes(2),
                    Description = "desc",
                    UpdateDate = date,
                    CreateDate = DateTime.Now,
                };
                var tender2 = new TenderModel
                {
                    ID = 2,
                    UserID = 2,
                    Title = "title-2",
                    Reference = "ref-2",
                    ReleaseDate = date.AddMinutes(1),
                    ClosingDate = date.AddMinutes(2),
                    Description = "desc",
                    UpdateDate = date,
                    CreateDate = DateTime.Now,
                };

                result.Add(context.Tender.Add(tender1));
                result.Add(context.Tender.Add(tender2));

                context.SaveChanges();
            }

            return result;
        }
    }
}
