﻿namespace TestCaseBackendDeveloper_v._3.Errors
{
    public class Error
    {
        public string Code;
        public string details;
        public string Id;

        public Error()
        {
            Code = null;
            Id = null;
            details = null;
        }
    }
}
