﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.Errors
{
    public class ErrorUtil
    {
        public static readonly Error UserIDNotFound = new Error() { Id = "10003", Code = "Player ID Not Found" };
        public static readonly Error InvalidLoginAttempt = new Error() { Id = "10007", Code = "Invalid Login Attempt" };
        public static readonly Error InvalidModelState = new Error() { Id = "10008", Code = "Invalid Model State" };
        public static readonly Error InvalidDateInput = new Error() { Id = "10009", Code = "Invalid Date Input" };
        public static readonly Error InternalServerError = new Error() { Id = "19999", Code = "Internal server error" };
        public static readonly Error QueryOrderByNotMatch = new Error() { Id = "17001", Code = "Query OrderBy Not Match" };
    }
}
