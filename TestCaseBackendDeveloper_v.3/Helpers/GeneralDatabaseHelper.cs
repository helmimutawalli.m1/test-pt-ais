﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Extensions;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Query.General;

namespace TestCaseBackendDeveloper_v._3.Helpers
{
    public static class GeneralDatabaseHelper
    {
        public static async Task<bool> SaveDatabase(DatabaseContext databaseContext, ILogger logger, string methodName)
        {
            try
            {
                if (await databaseContext.SaveChangesAsync() < 1)
                {
                    logger.AisLogError(ErrorUtil.InternalServerError,
                          HttpStatusCode.InternalServerError,
                          $"[{methodName}]" +
                          $"[{ErrorUtil.InternalServerError.Code}]" +
                          $"Failed save to server");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.AisLogCritical(ErrorUtil.InternalServerError,
                          HttpStatusCode.InternalServerError,
                          $"[{methodName}]" +
                          $"[{ErrorUtil.InternalServerError.Code}]" +
                          $"Failed save to server" +
                          $"[Exception]" +
                          $"{e.InnerException}");
                return false;
            }

            return true;
        }

        public static async Task<(bool,string)> SaveDatabaseTest(DatabaseContext databaseContext, ILogger logger, string methodName)
        {
            try
            {
                if (await databaseContext.SaveChangesAsync() < 1)
                {
                    logger.AisLogError(ErrorUtil.InternalServerError,
                          HttpStatusCode.InternalServerError,
                          $"[{methodName}]" +
                          $"[{ErrorUtil.InternalServerError.Code}]" +
                          $"Failed save to server");
                    return (true, ErrorUtil.InternalServerError.Code);
                }
            }
            catch (Exception e)
            {
                logger.AisLogCritical(ErrorUtil.InternalServerError,
                          HttpStatusCode.InternalServerError,
                          $"[{methodName}]" +
                          $"[{ErrorUtil.InternalServerError.Code}]" +
                          $"Failed save to server" +
                          $"[Exception]" +
                          $"{e.InnerException}");
                return (false, $"{e.InnerException}");
            }

            return (true, null);
        }

        /// <summary>
        /// This method will help build a predicate for database query based on similiar comparable data
        /// </summary>
        /// <param name="builder"></param>
        /// <returns> Predicate for Database Query </returns>
        public static Expression<Func<TDatabaseModel, bool>> CreateCustomExpressionForDBQuery<TDatabaseModel>(Action<CustomPredicateForDBQueryBuilder<TDatabaseModel>> builder)
        {

            List<BinaryExpression> _expressions = new List<BinaryExpression>();

            var dbModelExpression = Expression.Parameter(typeof(TDatabaseModel), "dbmodel");

            CustomPredicateForDBQueryBuilder<TDatabaseModel> _builder = new CustomPredicateForDBQueryBuilder<TDatabaseModel>(_expressions, dbModelExpression);

            builder(_builder);

            if (_expressions.Count == 0)
            {
                return Expression.Lambda<Func<TDatabaseModel, bool>>(
                    Expression.Constant(true),
                    dbModelExpression
                );
            }

            var result = _expressions[0];
            for (int i = 1; i < _expressions.Count; i++)
            {
                result = Expression.And(result, _expressions[i]);
            }

            return Expression.Lambda<Func<TDatabaseModel, bool>>(result, dbModelExpression);

        }

        public class CustomPredicateForDBQueryBuilder<TDatabaseModel>
        {
            private List<BinaryExpression> _expressions;
            private ParameterExpression _dbModelExpression;

            public CustomPredicateForDBQueryBuilder(List<BinaryExpression> expressions, ParameterExpression dbModelExpression)
            {
                _expressions = expressions;
                _dbModelExpression = dbModelExpression;
            }

            public void AddComparable(IComparable queryData, string dbModelVariableName)
            {
                if (queryData != null)
                {
                    var qdata = Expression.Constant(queryData);
                    var dbdata = Expression.Property(_dbModelExpression, dbModelVariableName);

                    _expressions.Add(Expression.Equal(qdata, dbdata));
                };
            }

            public void AddTimeBetween(TimeBetweenQuery queryData, string dbModelVariableName)
            {
                TimeBetweenQuery tbq = queryData;
                if (tbq != null)
                {
                    var dbdata = Expression.Property(_dbModelExpression, dbModelVariableName);
                    if (tbq.Start != null)
                    {
                        var qdata = Expression.Constant(tbq.Start, typeof(DateTime));
                        _expressions.Add(Expression.GreaterThan(dbdata, qdata));
                    }
                    if (tbq.End != null)
                    {
                        var qdata = Expression.Constant(tbq.End, typeof(DateTime));
                        _expressions.Add(Expression.LessThan(dbdata, qdata));
                    }
                };
            }
        }

        /// <summary>
        /// This method will help build a predicate for database query based on similiar comparable data
        /// </summary>
        /// <param name="query"></param>
        /// <param name="order"></param>
        /// <param name="builder"></param>
        /// <returns> Predicate for Database Query </returns>
        public static (IOrderedQueryable<TDatabaseModel>, Error) CreateCustomOrderedQuery<TDatabaseModel>(IQueryable<TDatabaseModel> query, OrderQuery order, Action<CustomOrderedQuery<TDatabaseModel, IComparable>> builder)
        where TDatabaseModel : BaseModel
        {
            Expression<Func<TDatabaseModel, long>> defaultExpression = (tdm) => tdm.ID;
            if (order == null)
            {
                return (query.OrderBy(defaultExpression), null);
            }
            else if (order.OrderBy == null && order.IsDescending)
            {
                return (query.OrderByDescending(defaultExpression), null);
            }
            else if (order.OrderBy == null && !order.IsDescending)
            {
                return (query.OrderBy(defaultExpression), null);
            }


            Dictionary<string, Func<TDatabaseModel, IComparable>> methods = new Dictionary<string, Func<TDatabaseModel, IComparable>>();

            CustomOrderedQuery<TDatabaseModel, IComparable> _builder = new CustomOrderedQuery<TDatabaseModel, IComparable>(methods);
            builder(_builder);

            foreach (var orderBy in methods)
            {
                if (order.OrderBy.Equals(orderBy.Key, StringComparison.InvariantCultureIgnoreCase))
                {
                    Expression<Func<TDatabaseModel, IComparable>> orderByExpression = (param) => orderBy.Value(param);
                    if (order.IsDescending)
                    {
                        return (query.OrderByDescending(orderByExpression), null);
                    }
                    else
                    {
                        return (query.OrderBy(orderByExpression), null);
                    }
                }
            }

            return (null, ErrorUtil.QueryOrderByNotMatch);
        }

        public class CustomOrderedQuery<TDatabaseModel, IComparable>
        {
            private Dictionary<string, Func<TDatabaseModel, IComparable>> _dictionary;

            public CustomOrderedQuery(Dictionary<string, Func<TDatabaseModel, IComparable>> dictionary)
            {
                _dictionary = dictionary;
            }

            public void AddPossibleOrderBy(string methodName, Func<TDatabaseModel, IComparable> keySelector)
            {
                _dictionary.Add(methodName, keySelector);
            }
        }
    }
}
