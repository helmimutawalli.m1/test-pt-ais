using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Configurations;
using TestCaseBackendDeveloper_v._3.Constants;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Services.Auth;
using TestCaseBackendDeveloper_v._3.Services.Tender;
using TestCaseBackendDeveloper_v._3.Services.User;

namespace TestCaseBackendDeveloper_v._3
{
    public class Startup
    {
        private IApiVersionDescriptionProvider _provider;
        protected IMvcCoreBuilder MvcCoreBuilder { get; private set; }


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();

            services.Configure<JWTOptions>(Configuration.GetSection("JWTOptions"));
            #region Database Connection
            var connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<DatabaseContext>(builder => builder.UseSqlServer(connection));
            #endregion

            #region MVC

            services.Configure<MvcOptions>(opt => { opt.RequireHttpsPermanent = false; });

            #endregion

            #region Roles

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AisConstants.POLICY_NAME_SUPER_ADMIN, policy =>
                {
                    policy.RequireClaim(AisConstants.CLAIM_ROLE_KEY, AisConstants.CLAIM_ROLE_VALUE_SUPER_ADMIN, AisConstants.CLAIM_ROLE_VALUE_ADMIN);
                });
                options.AddPolicy(AisConstants.POLICY_NAME_ADMIN, policy =>
                {
                    policy.RequireClaim(AisConstants.CLAIM_ROLE_KEY, AisConstants.CLAIM_ROLE_VALUE_ADMIN);
                });
                options.AddPolicy(AisConstants.POLICY_NAME_USER, policy =>
                {
                    policy.RequireClaim(AisConstants.CLAIM_ROLE_KEY, AisConstants.CLAIM_ROLE_VALUE_USER);
                });
            });

            #endregion

            #region Service Injection
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<IUserHandler, UserHandler>();
            services.AddScoped<ITenderHandler, TenderHandler>();
            services.AddScoped<IAuthHandler, AuthHandler>();
            #endregion
            
            #region API Versioning

            // Do not change the builder invocation because it might be sequence dependent
            MvcCoreBuilder = services.AddMvcCore();
            MvcCoreBuilder.SetCompatibilityVersion(CompatibilityVersion.Latest);
            MvcCoreBuilder.AddApiExplorer();
            MvcCoreBuilder.AddAuthorization();

            //MvcCoreBuilder.AddJsonFormatters();

            MvcCoreBuilder.AddVersionedApiExplorer(
                options =>
                {
                    options.GroupNameFormat = "'v'VVV";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;
                });

            services.AddApiVersioning(o =>
            {
#if !RELEASE
                o.ReportApiVersions = true;
#endif
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
                o.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });
            #endregion

            #region Swagger
            _provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
#if (!RELEASE && !SANDBOX)

            services
                .AddSwaggerGen(
                    options =>
                    {
                        // add a swagger document for each discovered API version
                        // note: you might choose to skip or document deprecated API versions differently
                        foreach (var description in _provider.ApiVersionDescriptions)
                            options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));

                        // add a custom operation filter which sets default values
                        //options.OperationFilter<SwaggerDefaultValues>();

                        // integrate xml comments
                        options.IncludeXmlComments(XmlCommentsFilePath);
                    }
                    );
#endif
            #endregion

            #region configure jwt authentication
            var JWTOptions = Configuration.GetSection("JWTOptions").Get<JWTOptions>();

            var key = Encoding.ASCII.GetBytes(JWTOptions.JWTSecret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero

                };
            })
            .AddCookie(o =>
            {
                o.Cookie.HttpOnly = true;
                o.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            });

#endregion
            
            services.AddMvc();

            #region Health Check
            services.AddHealthChecks()
                .AddCheck<TestCaseBackendDeveloper_v._3.Middlewares.HealthCheckMiddleware>("app_health_check")
                .AddDbContextCheck<DatabaseContext>()
                .AddSqlServer(connection, null, "AnalyticDBContext", HealthStatus.Degraded)
                ;
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

#if (!RELEASE && !SANDBOX)
            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // build a swagger endpoint for each discovered API version
                    foreach (var description in _provider.ApiVersionDescriptions)
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                            description.GroupName.ToUpperInvariant());
                }
                );

#endif


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health", new HealthCheckOptions()
                {
                    ResultStatusCodes =
                    {
                        [HealthStatus.Healthy] = StatusCodes.Status200OK,
                        [HealthStatus.Degraded] = StatusCodes.Status400BadRequest,
                        [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
                    },
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });

                endpoints.MapControllers();
            });
        }

        private string XmlCommentsFilePath
        {
            get
            {
                var basePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                var fileName = Assembly.GetEntryAssembly().GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }

        private OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new OpenApiInfo
            {
                Title = "Server",
                Description = "Server backend features",
            };

            if (description.IsDeprecated) info.Description += " This API version has been deprecated.";

            return info;
        }
    }
}
