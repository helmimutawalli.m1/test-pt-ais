﻿using System;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Query;
using TestCaseBackendDeveloper_v._3.Query.General;

namespace TestCaseBackendDeveloper_v._3.Message
{
    public class TenderMessage
    {
        public TenderQuery Query { get; set; }
        public OrderQuery Order { get; set; }
        public LimitQuery Limit { get; set; }
    }

    public class TenderAddMessage
    {
        public string Title { get; set; }
        public string Reference { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public string Description { get; set; }
    }
    public class TenderEditMessage
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string Reference { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public string Description { get; set; }
    }
}
