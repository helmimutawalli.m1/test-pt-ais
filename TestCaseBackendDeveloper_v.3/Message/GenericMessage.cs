﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestCaseBackendDeveloper_v._3.Message
{
    public class GenericMessage<T>
    {
        [Required]
        public T Data { get; set; }
        public IEnumerable<GenericData> Meta { get; set; }
    }

    public class GenericData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
