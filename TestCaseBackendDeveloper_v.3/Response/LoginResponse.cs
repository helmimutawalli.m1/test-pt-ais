﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.Response
{
    public class LoginResponse : BasicResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public int TokenDuration { get; set; }
    }
}
