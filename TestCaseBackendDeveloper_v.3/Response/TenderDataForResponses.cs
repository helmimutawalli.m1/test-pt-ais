﻿using System;
using TestCaseBackendDeveloper_v._3.Models;

namespace TestCaseBackendDeveloper_v._3.Response
{
    public class TenderDataForResponses
    {
        public long UserId { get; set; }
        public UserModel User { get; set; }
        public long ID { get; set; }
        public string Title { get; set; }
        public string Reference { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }

        public static TenderDataForResponses Create(TenderModel model)
        {
            TenderDataForResponses result = new TenderDataForResponses();
            result.UserId = model.UserID;
            if(model.UserModel != null)
            {
                result.User = model.UserModel;
            }
            result.ID = model.ID;
            result.Title = model.Title;
            result.Reference = model.Reference;
            result.ReleaseDate = model.ReleaseDate;
            result.ClosingDate = model.ClosingDate;
            result.Description = model.Description;
            return result;
        }

    }
}
