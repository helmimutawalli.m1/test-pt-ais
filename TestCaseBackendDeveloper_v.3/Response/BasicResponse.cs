﻿using System.Net;
using TestCaseBackendDeveloper_v._3.Errors;

namespace TestCaseBackendDeveloper_v._3.Response
{
    public class BasicResponse
    {
        public Error Error { get; set; } = null;
        public string Message { get; set; } = "ok";
        public HttpStatusCode StatusCode { get; set; }
    }
}
