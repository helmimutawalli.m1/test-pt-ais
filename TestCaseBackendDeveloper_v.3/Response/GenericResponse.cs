﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestCaseBackendDeveloper_v._3.Message;

namespace TestCaseBackendDeveloper_v._3.Response
{
    public class GenericResponse<T> : BasicResponse
    {
        [Required]
        public T Data { get; set; }
        public IEnumerable<GenericData> Meta { get; set; }
    }
}
