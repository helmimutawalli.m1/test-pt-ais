﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Helpers;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Response;
using TestCaseBackendDeveloper_v._3.Services.Auth;

namespace TestCaseBackendDeveloper_v._3.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AuthController : ControllerBase
    {

        private readonly DatabaseContext _databaseContext;
        private readonly IAuthHandler _authHandler;
        private readonly ILogger _logger;

        public AuthController(DatabaseContext databaseContext, IAuthHandler authHandler, ILoggerFactory logger)
        {
            _databaseContext = databaseContext;
            _authHandler = authHandler;
            _logger = logger.CreateLogger<AuthController>();
        }

        /// <summary>
        /// User Login
        /// </summary>
        /// <response code="400">
        /// { Id = "10003", Code = "User ID Not Found" }
        /// { Id = "10007", Code = "Invalid Login Attempt" }
        /// </response> 
        [HttpPost]
        [ProducesResponseType(typeof(BasicResponse), 400)]
        [ProducesResponseType(typeof(LoginResponse), 200)]
        public async Task<IActionResult> Login([FromBody] LoginMessage message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new BasicResponse() { Error = ErrorUtil.InvalidModelState });
            }

            var (user, tokenDuration, error) = await _authHandler.Login(message.Email, message.Password);
            if (error != null)
            {
                return BadRequest(new BasicResponse() { Error = error });
            }

            var (success, msg) = await GeneralDatabaseHelper.SaveDatabaseTest(_databaseContext, _logger, nameof(Login));
            if (!success) return BadRequest(new BasicResponse() { Error = ErrorUtil.InternalServerError,Message= msg });

            return Ok(new LoginResponse()
            {
                UserId = user.UserId,
                Token = user.Token,
                TokenDuration = tokenDuration
            });
        }

    }
}
