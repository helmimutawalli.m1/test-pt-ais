﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Attribute;
using TestCaseBackendDeveloper_v._3.Constants;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Helpers;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Response;
using TestCaseBackendDeveloper_v._3.Services.Tender;

namespace TestCaseBackendDeveloper_v._3.Controllers
{
    /// <summary>
    /// Controller to Authentication
    /// </summary>
    [AisAuthorize(AisConstants.POLICY_NAME_ADMIN)]
    [ApiController]
    [Route("Tender/[action]")]
    public class TenderController : ControllerBase
    {

        private readonly DatabaseContext _databaseContext;
        private readonly ITenderHandler _tenderHandler;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger _logger;

        public TenderController(DatabaseContext databaseContext, IHttpContextAccessor httpContextAccessor, ITenderHandler tenderHandler, ILoggerFactory logger)
        {
            _tenderHandler = tenderHandler;
            _httpContextAccessor = httpContextAccessor;
            _databaseContext = databaseContext;
            _logger = logger.CreateLogger<TenderController>();
        }

        /// <summary>
        /// Read Tender Data
        /// </summary>
        /// <response code="400">
        /// { Id = "17001", Code = "Query OrderBy Not Match" }
        /// </response> 
        [HttpPost]
        [ProducesResponseType(typeof(BasicResponse), 400)]
        [ProducesResponseType(typeof(GenericResponse<IEnumerable<TenderDataForResponses>>), 200)]
        public async Task<IActionResult> Read([FromBody] TenderMessage message)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new BasicResponse() { Error = ErrorUtil.InvalidModelState });
                }

                var (tenders, error) = await _tenderHandler.FindAllTender(message.Query, message.Order, message.Limit);
                var total = await _tenderHandler.TotalAllTender(message.Query);

                if (error != null)
                {
                    return BadRequest(new BasicResponse() { Error = error });
                }

                return Ok(new GenericResponse<IEnumerable<TenderDataForResponses>>()
                {
                    Data = tenders.ToList().ConvertAll(a => TenderDataForResponses.Create(a)),
                    Meta = new List<GenericData>()
                            {
                                new GenericData() {
                                    Key = "Total",
                                    Value = total.ToString()
                                }
                            }
                });
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(new BasicResponse() { Error = ErrorUtil.InternalServerError });
            }
            
        }


        /// <summary>
        /// Create Tender
        /// </summary>
        /// <response code="400">
        /// { Id = "10103", Code = "Id of the model in the message already exists in the database" };
        /// { Id = "10007", Code = "Invalid Login Attempt" }
        /// </response> 
        [HttpPost]
        [ProducesResponseType(typeof(BasicResponse), 400)]
        [ProducesResponseType(typeof(TenderDataForResponses), 200)]
        public async Task<IActionResult> Create([FromBody] TenderAddMessage message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new BasicResponse() { Error = ErrorUtil.InvalidModelState });
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var (tender, error) = await _tenderHandler.CreateTender(userId, message);
            if (error != null) return BadRequest(new BasicResponse() { Error = error });

            return Ok(new GenericResponse<TenderDataForResponses>()
            {
                Data = TenderDataForResponses.Create(tender)
            });
        }

        /// <summary>
        /// Update Tender
        /// </summary>
        /// <response code="400">
        /// { Id = "10103", Code = "Id of the model in the message already exists in the database" };
        /// { Id = "10007", Code = "Invalid Login Attempt" }
        /// </response> 
        [HttpPut]
        [ProducesResponseType(typeof(BasicResponse), 400)]
        [ProducesResponseType(typeof(TenderDataForResponses), 200)]
        public async Task<IActionResult> Edit([FromBody] TenderEditMessage message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new BasicResponse() { Error = ErrorUtil.InvalidModelState });
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var (tender, error) = await _tenderHandler.EditTender(userId, message);
            if (error != null) return BadRequest(new BasicResponse() { Error = error });

            return Ok(new GenericResponse<TenderDataForResponses>()
            {
                Data = TenderDataForResponses.Create(tender)
            });
        }

        /// <summary>
        /// Delete Tender
        /// </summary>
        /// <response code="400">
        /// { Id = "10103", Code = "Id of the model in the message already exists in the database" };
        /// { Id = "10007", Code = "Invalid Login Attempt" }
        /// </response> 
        [HttpDelete]
        [ProducesResponseType(typeof(BasicResponse), 400)]
        [ProducesResponseType(typeof(TenderDataForResponses), 200)]
        public async Task<IActionResult> Delete(string tenderId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new BasicResponse() { Error = ErrorUtil.InvalidModelState });
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var error = await _tenderHandler.DeleteTender(userId, tenderId);
            if (error != null) return BadRequest(new BasicResponse() { Error = error });

            return Ok(new GenericResponse<TenderDataForResponses>()
            {
                Data = null
            });
        }
    }
}
