﻿using System;

namespace TestCaseBackendDeveloper_v._3.Models
{
    public class UserModel : BaseModel
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public DateTime LastRefresh { get; set; }
    }
}
