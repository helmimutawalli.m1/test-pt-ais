﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestCaseBackendDeveloper_v._3.Models
{
    public class BaseModel
    {
        //[Column(TypeName = "bigint(20)")]
        public long ID { get; set; }
        /// <summary>
        ///     Created data entry Date
        /// </summary>
        //[Column(TypeName = "datetime(6)")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        ///     Last Updated data entry Date
        /// </summary>
        //[Column(TypeName = "datetime(6)")]
        public DateTime UpdateDate { get; set; }
    }
}
