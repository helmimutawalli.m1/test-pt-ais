﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestCaseBackendDeveloper_v._3.Models
{
    public class TenderModel : BaseModel
    {
        /// </summary>
        [Required]
        [ForeignKey("UserModel")]
        public long UserID { get; set; }
        [ForeignKey("UserID")]
        public UserModel UserModel { get; set; }

        public string Title { get; set; }
        public string Reference { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
    }
}
