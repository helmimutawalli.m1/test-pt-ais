﻿using TestCaseBackendDeveloper_v._3.Query.General;

namespace TestCaseBackendDeveloper_v._3.Query
{
    public class TenderQuery
    {
        public long ID { get; set; }
        public long UserID { get; set; }
        public string Title { get; set; }
        public TimeBetweenQuery ReleaseDate { get; set; }
        public TimeBetweenQuery ClosingDate { get; set; }
    }
}
