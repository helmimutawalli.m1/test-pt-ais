﻿using System;

namespace TestCaseBackendDeveloper_v._3.Query.General
{
    public class TimeBetweenQuery
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }
}
