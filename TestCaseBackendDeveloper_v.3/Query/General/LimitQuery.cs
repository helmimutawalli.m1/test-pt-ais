﻿namespace TestCaseBackendDeveloper_v._3.Query.General
{
    public class LimitQuery
    {
        public int? StartCount { get; set; }
        public int? EndCount { get; set; }

        public static readonly int DefaultStartCount = 0;
        public static readonly int DefaultEndCount = 10;

        public static LimitQuery CreateAsDefault()
        {
            return new LimitQuery()
            {
                StartCount = DefaultStartCount,
                EndCount = DefaultEndCount
            };
        }

        public void FixAsDefault()
        {
            if (StartCount == null)
            {
                if (EndCount != null && EndCount - DefaultEndCount >= 0)
                {
                    StartCount = EndCount - DefaultEndCount;
                }
                else
                {
                    StartCount = DefaultStartCount;
                }
            }

            if (EndCount == null)
            {
                if (StartCount != null)
                {
                    EndCount = StartCount + DefaultEndCount;
                }
                else
                {
                    EndCount = DefaultEndCount;
                }
            }
        }

        public bool IsOnlyOneAndFirst()
        {
            return StartCount == 0 && EndCount == 1;
        }
    }
}
