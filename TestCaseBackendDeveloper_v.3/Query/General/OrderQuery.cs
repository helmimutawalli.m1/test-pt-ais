﻿namespace TestCaseBackendDeveloper_v._3.Query.General
{
    public class OrderQuery
    {
        public string OrderBy { get; set; }
        public bool IsDescending { get; set; }
    }
}
