﻿using Microsoft.AspNetCore.Authorization;

namespace TestCaseBackendDeveloper_v._3.Attribute
{
    public class AisAuthorizeAttribute : AuthorizeAttribute
    {
        public AisAuthorizeAttribute() : base()
        {
            AuthenticationSchemes = "Bearer";
        }

        public AisAuthorizeAttribute(string policy) : base(policy)
        {
            AuthenticationSchemes = "Bearer";
        }
    }
}
