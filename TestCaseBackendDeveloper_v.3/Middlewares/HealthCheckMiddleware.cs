﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.Middlewares
{
    public class HealthCheckMiddleware : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            bool StartupTaskCompleted = false;
            string projectName = Assembly.GetEntryAssembly().GetName().Name;
            if (projectName != null)
            {
                StartupTaskCompleted = true;
            }
            if (StartupTaskCompleted)
            {
                return Task.FromResult(HealthCheckResult.Healthy($"Application {projectName} is healthy"));
            }
            return Task.FromResult(HealthCheckResult.Unhealthy($"Appliation {projectName} is unhealthy"));
        }
    }
}
