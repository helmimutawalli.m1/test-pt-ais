﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Errors;

namespace TestCaseBackendDeveloper_v._3.Extensions
{
    public static class LoggingExtensions
    {
        public static void AisLogError(this ILogger logger, Error error, HttpStatusCode statusCode, string logMessage,
            [CallerMemberName] string methodName = "")
        {
            logger.LogError($"[{methodName}]" +
                            $"[{statusCode.ToString()}]" +
                            $"[{error.Id}]" +
                            $"[{error.Code}]" +
                            $"{logMessage}");
        }
        public static void AisLogCritical(this ILogger logger, Error error, HttpStatusCode statusCode,
            string logMessage, [CallerMemberName] string methodName = "")
        {
            logger.LogCritical($"[{methodName}]" +
                               $"[{statusCode.ToString()}]" +
                               $"[{error.Id}]" +
                               $"[{error.Code}]" +
                               $"{logMessage}");
        }
    }
}
