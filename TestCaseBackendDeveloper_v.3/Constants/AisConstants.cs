﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.Constants
{
    public class AisConstants
    {
        public const string POLICY_NAME_SUPER_ADMIN = "SuperAdmin";
        public const string POLICY_NAME_ADMIN = "Admin";
        public const string POLICY_NAME_USER = "User";

        public const string CLAIM_ROLE_KEY = "e0d85196-e0bb-417c-82a0-a075d1f512db";
        public const string CLAIM_ROLE_VALUE_SUPER_ADMIN = "21dcd0a7-3264-43e2-a44f-594ae48a93bd";
        public const string CLAIM_ROLE_VALUE_ADMIN = "5b44a2d8-2652-4e07-b971-382391ed9c06";
        public const string CLAIM_ROLE_VALUE_USER = "0dcd8329-d39e-4d02-9f8c-16f2ae62bcfd";
    }
}
