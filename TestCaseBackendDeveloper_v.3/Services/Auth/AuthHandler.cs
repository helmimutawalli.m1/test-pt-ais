﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Configurations;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Services.User;
using TestCaseBackendDeveloper_v._3.Utils;

namespace TestCaseBackendDeveloper_v._3.Services.Auth
{
    public class AuthHandler : IAuthHandler
    {
        private readonly DatabaseContext _databaseContext;
        private readonly IUserHandler _userHandler;
        private readonly JWTOptions _jwtOptions;
        private readonly ILogger _logger;

        public AuthHandler(DatabaseContext databaseContext, IUserHandler userHandler, IOptions<JWTOptions> option, ILoggerFactory logger)
        {
            _databaseContext = databaseContext;
            _userHandler = userHandler;
            _jwtOptions = option.Value;
            _logger = logger.CreateLogger<AuthHandler>();
        }

        public async Task<(UserModel, int, Error)> Login(string email, string password)
        {
            var selectedUser = await _userHandler.FindUser((user) => user.Email == email);
            if (selectedUser == null) return (null, 0, ErrorUtil.UserIDNotFound);

            if (!PasswordHash.ValidatePassword(password, selectedUser.Password))
            {
                return (null, 0, ErrorUtil.InvalidLoginAttempt);
            }

            selectedUser.Token = JwtTokenGenerator.CreateToken(selectedUser.UserId, selectedUser.Role, _jwtOptions.JWTSecret, _jwtOptions.TokenDuration);

            selectedUser.LastRefresh = DateTime.Now;

            return (selectedUser, _jwtOptions.TokenDuration, null);
        }
    }
}
