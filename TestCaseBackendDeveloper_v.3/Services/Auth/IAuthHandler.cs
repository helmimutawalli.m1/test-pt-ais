﻿using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Models;

namespace TestCaseBackendDeveloper_v._3.Services.Auth
{
    public interface IAuthHandler
    {
        Task<(UserModel, int, Error)> Login(string email, string password);
    }
}
