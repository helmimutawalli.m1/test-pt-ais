﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Models;

namespace TestCaseBackendDeveloper_v._3.Services.User
{
    public class UserHandler : IUserHandler
    {
        private readonly DatabaseContext _databaseContext;
        private readonly ILogger _logger;

        public UserHandler(DatabaseContext databaseContext, ILoggerFactory logger)
        {
            _databaseContext = databaseContext;
            _logger = logger.CreateLogger<UserHandler>();
        }

        public async Task<UserModel> FindUser(Expression<Func<UserModel, bool>> exp)
        {
            return await _databaseContext.User.Where(exp).OrderBy(user => user.ID).FirstOrDefaultAsync();
        }

    }
}
