﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Models;

namespace TestCaseBackendDeveloper_v._3.Services.User
{
    public interface IUserHandler
    {
        Task<UserModel> FindUser(Expression<Func<UserModel, bool>> exp);
    }
}
