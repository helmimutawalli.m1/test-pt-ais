﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Query;
using TestCaseBackendDeveloper_v._3.Query.General;

namespace TestCaseBackendDeveloper_v._3.Services.Tender
{
    public interface ITenderHandler
    {
        Task<(IEnumerable<TenderModel>, Error)> FindAllTender(TenderQuery tenderQuery, OrderQuery order, LimitQuery limit);
        Task<int> TotalAllTender(TenderQuery tenderQuery);
        Task<(TenderModel, Error)> CreateTender(string userId, TenderAddMessage message);
        Task<(TenderModel, Error)> EditTender(string userId, TenderEditMessage message);
        Task<Error> DeleteTender(string userId, string tenderId);
    }
}
