﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Errors;
using TestCaseBackendDeveloper_v._3.Extensions;
using TestCaseBackendDeveloper_v._3.Helpers;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Query;
using TestCaseBackendDeveloper_v._3.Query.General;
using TestCaseBackendDeveloper_v._3.Services.User;

namespace TestCaseBackendDeveloper_v._3.Services.Tender
{
    public class TenderHandler : ITenderHandler
    {

        private readonly DatabaseContext _databaseContext;
        private readonly IUserHandler _userHandler;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger _logger;

        public TenderHandler(DatabaseContext databaseContext, IHttpContextAccessor httpContextAccessor, IUserHandler userHandler, ILoggerFactory logger)
        {
            _databaseContext = databaseContext;
            _userHandler = userHandler;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger.CreateLogger<TenderHandler>();
        }

        public async Task<TenderModel> FindTender(Expression<Func<TenderModel, bool>> expression)
        {
            return await _databaseContext.Tender.Include(i => i.UserModel).Where(expression).OrderBy(t => t.ID).FirstOrDefaultAsync();
        }
        public async Task<(IEnumerable<TenderModel>, Error)> FindAllTender(Expression<Func<TenderModel, bool>> expression, OrderQuery order, LimitQuery limit)
        {
            if (limit == null)
            {
                limit = LimitQuery.CreateAsDefault();
            }
            else
            {
                limit.FixAsDefault();
            }

            if (limit.IsOnlyOneAndFirst())
            {
                TenderModel selectedTender = await FindTender(expression);
                return (new TenderModel[1] { selectedTender }, null);
            }

            IQueryable<TenderModel> tenderQuery = _databaseContext.Tender.Include(i => i.UserModel).Where(expression);

            var desc = false;
            var orderData = order == null ? "" : order.OrderBy;
            var isDescending = order == null ? desc : order.IsDescending;
            var orderedTenderQuery = tenderQuery.OrderByDynamic(orderData, isDescending ? LinqExtensions.Order.Desc : LinqExtensions.Order.Asc);

            return (await orderedTenderQuery.Take((int)limit.EndCount).Skip((int)limit.StartCount).ToListAsync(), null);
        }

        public async Task<(IEnumerable<TenderModel>, Error)> FindAllTender(TenderQuery tenderQuery, OrderQuery order, LimitQuery limit)
        {
            var exp = CreateDbQuery(tenderQuery);
            return await FindAllTender(exp, order, limit);
        }

        public async Task<int> TotalAllTender(TenderQuery tenderQuery)
        {
            var exp = CreateDbQuery(tenderQuery);
            return await _databaseContext.Tender.Where(exp).CountAsync();
        }

        public async Task<(TenderModel, Error)> CreateTender(string userId, TenderAddMessage message)
        {
            var userData = await _userHandler.FindUser(f => f.UserId == userId);

            if ((DateTime.Now > message.ReleaseDate || DateTime.Now > message.ClosingDate) || message.ReleaseDate > message.ClosingDate) return (null, ErrorUtil.InvalidDateInput);

            var data = new TenderModel()
            {
                UserID = userData.ID,
                Title = message.Title,
                Reference = message.Reference,
                Description = message.Description,
                ReleaseDate = message.ReleaseDate,
                ClosingDate = message.ClosingDate
            };

            await _databaseContext.Tender.AddAsync(data);

            var success = await GeneralDatabaseHelper.SaveDatabase(_databaseContext, _logger, nameof(CreateTender));
            if (!success) return (null, ErrorUtil.InternalServerError);


            return (data, null);
        }

        public async Task<(TenderModel, Error)> EditTender(string userId, TenderEditMessage message)
        {
            var userData = await _userHandler.FindUser(f => f.UserId == userId);
            var tenderData = await FindTender(f => f.ID == message.ID);

            if ((DateTime.Now > message.ReleaseDate && DateTime.Now > message.ClosingDate) || message.ReleaseDate > message.ClosingDate) return (null, ErrorUtil.InternalServerError);

            tenderData.Title = message.Title;
            tenderData.Reference = message.Reference;
            tenderData.Description = message.Description;
            tenderData.ReleaseDate = message.ReleaseDate;
            tenderData.ClosingDate = message.ClosingDate;

            _databaseContext.Tender.Update(tenderData);

            var success = await GeneralDatabaseHelper.SaveDatabase(_databaseContext, _logger, nameof(EditTender));
            if (!success) return (null, ErrorUtil.InternalServerError);


            return (tenderData, null);
        }

        public async Task<Error> DeleteTender(string userId, string tenderId)
        {
            var userData = await _userHandler.FindUser(f => f.UserId == userId);
            var tenderData = await FindTender(f => f.ID.ToString() == tenderId);


            tenderData.IsDelete = true;

            _databaseContext.Tender.Update(tenderData);

            var success = await GeneralDatabaseHelper.SaveDatabase(_databaseContext, _logger, nameof(EditTender));
            if (!success) return ErrorUtil.InternalServerError;


            return null;
        }

        private Expression<Func<TenderModel, bool>> CreateDbQuery(TenderQuery tenderQuery)
        {
            return GeneralDatabaseHelper.CreateCustomExpressionForDBQuery<TenderModel>((builder) => {
                if (tenderQuery != null)
                {
                    if(tenderQuery.ID != 0) builder.AddComparable(tenderQuery.ID, nameof(TenderModel.ID));
                    if(tenderQuery.UserID != 0) builder.AddComparable(tenderQuery.UserID, nameof(TenderModel.UserID));
                    if (tenderQuery.Title != null) builder.AddComparable(tenderQuery.Title, nameof(TenderModel.Title));
                    if (tenderQuery.ReleaseDate != null) builder.AddTimeBetween(tenderQuery.ReleaseDate, nameof(TenderModel.ReleaseDate));
                    if (tenderQuery.ClosingDate != null) builder.AddTimeBetween(tenderQuery.ClosingDate, nameof(TenderModel.ClosingDate));
                    builder.AddComparable(false, nameof(TenderModel.IsDelete));
                }
            });
        }
    }
}
