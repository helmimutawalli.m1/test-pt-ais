﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HostFiltering;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog;
using Microsoft.Extensions.Hosting;
using System.Security.Authentication;

namespace TestCaseBackendDeveloper_v._3.Hosting
{
    public static class WebHost
    {
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var builder = new WebHostBuilder()
                .UseKestrel((builderContext, options) =>
                {
                    options.Configure(builderContext.Configuration.GetSection("Kestrel"));

                    options.ConfigureHttpsDefaults(s =>
                    {
                        s.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls13;
                    });
                })
                .CaptureStartupErrors(true)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
#if RELEASE
                    env.EnvironmentName = EnvironmentName.Release;
#elif SANDBOX
                    env.EnvironmentName = EnvironmentName.Sandbox;
#elif STAGING
                    env.EnvironmentName = EnvironmentName.Staging;
#elif REVIEW
                    env.EnvironmentName = EnvironmentName.Review;
#elif DEVELOPMENT
                    env.EnvironmentName = EnvironmentName.Development;
#elif TESTING
                    env.EnvironmentName = EnvironmentName.Testing;
#else
                        env.EnvironmentName = EnvironmentName.Debug;
#endif

                    config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
#if !RELEASE
                    config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
#endif
                        //var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                        //if (appAssembly != null)
                        //{
                        //    config.AddUserSecrets(appAssembly, optional: true);
                        //}

                        config.AddEnvironmentVariables();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                })
                .UseSerilog((context, config) =>
                {
                    config.ReadFrom.Configuration(context.Configuration);

                    Log.Logger = new LoggerConfiguration()
                        .ReadFrom.Configuration(context.Configuration)
                        .CreateLogger();
                })
                //.ConfigureServices((hostingContext, services) =>
                //{
                //    // Fallback
                //    services.PostConfigure<HostFilteringOptions>(options =>
                //{
                //    if (options.AllowedHosts == null || options.AllowedHosts.Count == 0)
                //    {
                //            // "AllowedHosts": "localhost;127.0.0.1;[::1]"
                //            var hosts = hostingContext.Configuration["AllowedHosts"]?.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                //            // Fall back to "*" to disable.
                //            options.AllowedHosts = (hosts?.Length > 0 ? hosts : new[] { "*" });
                //    }
                //});
                //    // Change notification
                //    services.AddSingleton<IOptionsChangeTokenSource<HostFilteringOptions>>(
                //            new ConfigurationChangeTokenSource<HostFilteringOptions>(hostingContext.Configuration));

                //    services.AddTransient<IStartupFilter, HostFilteringStartupFilter>();
                //})
                .UseIISIntegration()
                .UseDefaultServiceProvider((context, options) =>
                {
                    options.ValidateScopes = context.HostingEnvironment.IsDevelopment();
                });

            if (args != null)
            {
                builder.UseConfiguration(new ConfigurationBuilder().AddCommandLine(args).Build());
            }

            return builder;
        }
        public static IWebHostBuilder CreateWebHostBuilder<TStartup>(string[] args) where TStartup : class =>
            CreateWebHostBuilder(args).UseStartup<TStartup>();
    }

    internal class HostFilteringStartupFilter : IStartupFilter
    {
        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            return app =>
            {
                app.UseHostFiltering();
                next(app);
            };
        }
    }


}
