﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.Configurations
{
    public class SecretOptions
    {
        public string JWTSecret { get; set; }

    }
}
