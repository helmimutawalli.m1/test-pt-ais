﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.Configurations
{
    public class JWTOptions
    {
        public string JWTSecret { get; set; }

        public int TokenDuration { get; set; }
    }
}
