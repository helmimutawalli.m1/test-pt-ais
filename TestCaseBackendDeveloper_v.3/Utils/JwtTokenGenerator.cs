﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Constants;

namespace TestCaseBackendDeveloper_v._3.Utils
{
    public static class JwtTokenGenerator
    {
        public static string CreateToken(string userId, string role, string secret, int duration)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);

            string value = "";
            switch (role)
            {
                case "Super Admin":
                    value = AisConstants.CLAIM_ROLE_VALUE_SUPER_ADMIN;
                    break;
                case "Admin":
                    value = AisConstants.CLAIM_ROLE_VALUE_ADMIN;
                    break;
                case "User":
                    value = AisConstants.CLAIM_ROLE_VALUE_USER;
                    break;
            }

            var jwtToken = new JwtSecurityToken
            (
                claims: new List<Claim>() {
                    new Claim(JwtRegisteredClaimNames.NameId, userId),
                    new Claim(AisConstants.CLAIM_ROLE_KEY, value)
                },
                expires: DateTime.UtcNow.AddMinutes(duration),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            );

            return tokenHandler.WriteToken(jwtToken); 
        }


    }
}
