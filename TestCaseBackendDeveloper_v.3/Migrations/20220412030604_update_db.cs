﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestCaseBackendDeveloper_v._3.Migrations
{
    public partial class update_db : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ClosingDate",
                table: "Tender",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Tender",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Tender",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Reference",
                table: "Tender",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReleaseDate",
                table: "Tender",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "UserID",
                table: "Tender",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tender_UserID",
                table: "Tender",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_Tender_User_UserID",
                table: "Tender",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tender_User_UserID",
                table: "Tender");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropIndex(
                name: "IX_Tender_UserID",
                table: "Tender");

            migrationBuilder.DropColumn(
                name: "ClosingDate",
                table: "Tender");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Tender");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Tender");

            migrationBuilder.DropColumn(
                name: "Reference",
                table: "Tender");

            migrationBuilder.DropColumn(
                name: "ReleaseDate",
                table: "Tender");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Tender");
        }
    }
}
