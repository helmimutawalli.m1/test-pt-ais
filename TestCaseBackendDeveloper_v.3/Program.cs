using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Hosting;

namespace TestCaseBackendDeveloper_v._3
{
    public class Program
    {
        Program()
        {

        }
        public static int Main(string[] args)
        {
            try
            {
                Console.WriteLine("Configurating analytic API host");

                var host = WebHost.CreateWebHostBuilder<Startup>(args).Build();

                Log.Information("Starting server...");

                //init DB
                using (var scope = host.Services.CreateScope())
                {
                    var services = scope.ServiceProvider;
                    var context = services.GetRequiredService<DatabaseContext>();
                    Log.Information("Initialize DB...");

                    DbInitializer.Initialize(context);

                    Log.Information("DB initialized...");
                }

                Log.Information("Running host...");
                host.Run();
                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Server terminated unexpectedly: {e.Message}");
                Console.WriteLine(e.StackTrace);
                Log.Fatal(e, "Server terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }


        }
    }
}
