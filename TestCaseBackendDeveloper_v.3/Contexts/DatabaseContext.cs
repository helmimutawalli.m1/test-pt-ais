﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Models;

namespace TestCaseBackendDeveloper_v._3.Contexts
{
    public class DatabaseContext : DbContext
    {

        public DbSet<TenderModel> Tender { get; set; }
        public DbSet<UserModel> User { get; set; }

        #region Store Procedure
        //public DbQuery<SpDbTenderData> SP_Tender { get; set; }

        #endregion

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
