﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Utils;

namespace TestCaseBackendDeveloper_v._3.Contexts
{
    public class DbInitializer
    {
        public static void Initialize(DatabaseContext context)
        {
            try
            {
                context.Database.Migrate();

                // add migration if any pending migrations of the context
                Console.WriteLine("Success Add Migration");
            }
            catch
            {
                Console.WriteLine("Failed Migration, No Migration Data");
            }
#if !RELEASE
            InitializeData(context);
#endif
            context.Database.EnsureCreated();
            context.SaveChanges();

        }

        public static void InitializeData(DatabaseContext context)
        {

            var userDataCheck = context.User.FirstOrDefault();
            if (userDataCheck == null)
            {
                
                var date = DateTime.Now;
                var user1 = new UserModel
                {
                    UserId = "user-1",
                    Email = "sa@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataSA!"),
                    Role = "Super Admin",
                    Username = "superadmin",
                    UpdateDate = date,
                    CreateDate = DateTime.Now,
                    Token = "",
                    LastRefresh = date
                };

                context.User.Add(user1);
                var user2 = new UserModel
                {
                    UserId = "user-2",
                    Email = "a@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataAdmin!"),
                    Role = "Admin",
                    Username = "admin",
                    CreateDate = date,
                    UpdateDate = date,
                    Token = "",
                    LastRefresh = date
                };
                context.User.Add(user2);
                var user3 = new UserModel
                {
                    UserId = "user-3",
                    Email = "u@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataUser!"),
                    Role = "User",
                    Username = "user",
                    CreateDate = date,
                    UpdateDate = date,
                    Token = "",
                    LastRefresh = date
                };
                context.User.Add(user3);

            }
            try
            {
                context.SaveChanges();
            }catch(Exception e)
            {
                Console.WriteLine("error: "+ e.Message);
                Console.WriteLine("error: "+ e.InnerException);

            }

        }
    }
}
