﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.Contexts
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        public DatabaseContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
#if DEBUG
                .AddJsonFile("appsettings.Debug.json")
#elif DEVELOPMENT
                .AddJsonFile("appsettings.Development.json")
#elif SANDBOX
                .AddJsonFile("appsettings.Sandbox.json")
#elif REVIEW
                .AddJsonFile("appsettings.Review.json")
#elif STAGING
                .AddJsonFile("appsettings.Staging.json")
#elif TESTING
                .AddJsonFile("appsettings.Testing.json")
#else
                .AddJsonFile("appsettings.json")
#endif
                .Build();

            var builder = new DbContextOptionsBuilder<DatabaseContext>();

            var connectionString = configuration.GetSection("ConnectionStrings:DefaultConnection");

            builder.UseSqlServer(connectionString.Get<string>(), b => b.MigrationsAssembly("TestCaseBackendDeveloper_v.3"));

            return new DatabaseContext(builder.Options);
        }
    }
}

