﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using TestCaseBackendDeveloper_v._3.Contexts;

namespace ServicesTestProject
{
    class ServiceTestDbInitializer
    {
        /// <summary>
        /// Database Context Instance
        /// </summary>
        public readonly DatabaseContext _context;



        public ServiceTestDbInitializer()
        {
            //Init database instance option to create instance in memory
            DbContextOptions<DatabaseContext> db_options = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                .EnableSensitiveDataLogging()
                .ConfigureWarnings(warn => warn.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            //Create DB context with previous options
            _context = new DatabaseContext(db_options);
        }

        public void InitializeEmptyData()
        {
            _context.Tender.RemoveRange(_context.Tender);
            _context.User.RemoveRange(_context.User);
           
            _context.SaveChanges();
        }


        // Initialize database
        public static void Initialize(DatabaseContext context)
        {
            context.Database.EnsureCreated();
            context.SaveChanges();
        }

    }
}
