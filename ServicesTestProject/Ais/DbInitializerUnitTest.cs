﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestCaseBackendDeveloper_v._3.Contexts;
using TestCaseBackendDeveloper_v._3.Models;
using TestCaseBackendDeveloper_v._3.Utils;

namespace ServicesTestProject.Ais
{
    public class DbInitializerUnitTest
    {
        public static List<EntityEntry<UserModel>> InitializeUser(DatabaseContext context)
        {
            List<EntityEntry<UserModel>> result = new List<EntityEntry<UserModel>>();

            if (!context.User.Any())
            {
                var date = DateTime.Now;

                var user1 = new UserModel
                {
                    UserId = "user-1",
                    Email = "sa@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataSA!"),
                    Role = "Super Admin",
                    Username = "superadmin",
                    UpdateDate = date,
                    CreateDate = DateTime.Now,
                    Token = "",
                    LastRefresh = date
                };

                var user2 = new UserModel
                {
                    UserId = "user-2",
                    Email = "a@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataAdmin!"),
                    Role = "Admin",
                    Username = "admin",
                    CreateDate = date,
                    UpdateDate = date,
                    Token = "",
                    LastRefresh = date
                };
                var user3 = new UserModel
                {
                    UserId = "user-3",
                    Email = "u@seeddata.com",
                    Password = PasswordHash.HashPassword("s33dDataUser!"),
                    Role = "User",
                    Username = "user",
                    CreateDate = date,
                    UpdateDate = date,
                    Token = "",
                    LastRefresh = date
                };
                result.Add(context.User.Add(user1));
                result.Add(context.User.Add(user2));
                result.Add(context.User.Add(user3));


                context.SaveChanges();
            }

            return result;
        }
    }
}
