﻿using Microsoft.AspNetCore.Mvc;
using TestCaseBackendDeveloper_v._3.Message;
using TestCaseBackendDeveloper_v._3.Response;
using Xunit;

namespace ServicesTestProject.Ais.User_Test
{
    public class UserControllerTestRead
    {
        [Fact]
        public void UserRead_Success()
        {
            UserReadTest(new LoginMessage {
                Email= "sa@seeddata.com",
                Password= "s33dDataSA!"
            });
        }

        public void UserReadTest(LoginMessage message)
        {
            var userController = UserControllerTestHelper.MakeController();
            var response = userController.Login(message).Result;

            var okResult = Assert.IsType<OkObjectResult>(response);
            var returnValue = Assert.IsType<LoginResponse>(okResult.Value);

            Assert.Equal("18", returnValue.UserId);
        }
    }
}
