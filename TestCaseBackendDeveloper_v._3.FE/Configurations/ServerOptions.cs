﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Configurations
{
    public class ServerOptions
    {
        public string ApiUrl { get; set; }
    }
}
