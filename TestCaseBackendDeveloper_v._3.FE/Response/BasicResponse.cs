﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Response
{
    public class BasicResponse
    {
        public Error Error { get; set; } = null;
        public string Message { get; set; } = "ok";
        public HttpStatusCode StatusCode { get; set; }
    }

    public class Error
    {
        public string Code;
        public string details;
        public string Id;

        public Error()
        {
            Code = null;
            Id = null;
            details = null;
        }
    }
}
