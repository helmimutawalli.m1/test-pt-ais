﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Response
{
    public class LoginResponse
    {
        /// <summary>
        /// User Id 
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Token 
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Token Duration
        /// </summary>
        public string TokenDuration { get; set; }
    }
}
