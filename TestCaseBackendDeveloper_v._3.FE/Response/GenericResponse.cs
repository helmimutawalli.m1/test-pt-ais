﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Response
{
    public class GenericResponse<T> : BasicResponse
    {
        public T Data { get; set; }
        public IEnumerable<GenericData> Meta { get; set; }
    }

    public class GenericData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
