﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using TestCaseBackendDeveloper_v._3.FE.Messages;
using TestCaseBackendDeveloper_v._3.FE.Models;
using TestCaseBackendDeveloper_v._3.FE.Response;
using TestCaseBackendDeveloper_v._3.FE.Utils;
using TestCaseBackendDeveloper_v._3.FE.ViewModel;

namespace TestCaseBackendDeveloper_v._3.FE.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientFactory _httpClient;

        public HomeController(IHttpClientFactory httpClient, ILogger<HomeController> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            //check login status
            if (HttpContext.Session.GetString("JWToken") == null)
            {
                return RedirectToAction("Login", "Home");
            }

            TenderMessage message = new TenderMessage
            {
                Query = new TenderQuery(),
                Order = new OrderQuery
                {
                    OrderBy = "ID",
                    IsDescending = true
                },
                Limit = new LimitQuery
                {
                    StartCount = 0,
                    EndCount = 100000000//get all, FIXME:not good
                }
            };

            var (response, responseStream) = await FetchTender(message);

            if (response.IsSuccessStatusCode)
            {
                var responseData = JsonConvert.DeserializeObject<GenericResponse<IEnumerable<TenderDataForResponses>>>(responseStream);

                ViewBag.TenderListData = responseData.Data;
            }

            return View();
        }

        public IActionResult Add()
        {
            //check login status
            if (HttpContext.Session.GetString("JWToken") == null)
            {
                return RedirectToAction("Login", "Home");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddTenderViewModel message)
        {
            //check login status
            if (HttpContext.Session.GetString("JWToken") == null)
            {
                return RedirectToAction("Login", "Home");
            }

            var client = _httpClient.CreateClient();

            var url = ApiUrls.BaseServerUrl + ApiUrls.AddTender;
            var token = HttpContext.Session.GetString("JWToken");
            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var response = await client.PostAsJsonAsync(url, message);

            var responseStream = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var responseData = JsonConvert.DeserializeObject<GenericResponse<TenderDataForResponses>>(responseStream);

                ViewBag.TenderListData = responseData.Data;
                return RedirectToAction("Index", "Home");

            }
            var responseDataError = JsonConvert.DeserializeObject<BasicResponse>(responseStream);
            ModelState.AddModelError(string.Empty, responseDataError.Error.Code);

            return View();

        }


        public async Task<IActionResult> Edit(string tenderId)
        {
            //check login status
            if (HttpContext.Session.GetString("JWToken") == null)
            {
                return RedirectToAction("Login", "Home");
            }

            await FetchTenderData(tenderId);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TenderDataForResponses model)
        {
            //check login status
            if (HttpContext.Session.GetString("JWToken") == null)
            {
                return RedirectToAction("Login", "Home");
            }

            var client = _httpClient.CreateClient();

            var message = new EditTenderViewModel()
            {
                ID = model.ID,
                Title = model.Title,
                Reference = model.Reference,
                ReleaseDate = model.ReleaseDate,
                ClosingDate = model.ClosingDate,
                Description = model.Description
            };

            var url = ApiUrls.BaseServerUrl + ApiUrls.UpdateTender;
            var token = HttpContext.Session.GetString("JWToken");
            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var response = await client.PutAsJsonAsync(url, message);

            var responseStream = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var responseData = JsonConvert.DeserializeObject<GenericResponse<TenderDataForResponses>>(responseStream);

                ViewBag.TenderListData = responseData.Data;
                return RedirectToAction("Index", "Home");

            }
            var responseDataError = JsonConvert.DeserializeObject<BasicResponse>(responseStream);
            ModelState.AddModelError(string.Empty, responseDataError.Error.Code);

            return View();

        }


        [Route("Detail")]
        public async Task<IActionResult> Detail(string tenderId)
        {
            //check login status
            if (HttpContext.Session.GetString("JWToken") == null)
            {
                return RedirectToAction("Login", "Home");
            }

            await FetchTenderData(tenderId);

            return View();
        }

        [Route("Delete")]
        public async Task<IActionResult> Delete(string tenderId)
        {
            //check login status
            if (HttpContext.Session.GetString("JWToken") == null)
            {
                return RedirectToAction("Login", "Home");
            }



            var client = _httpClient.CreateClient();

            var url = ApiUrls.BaseServerUrl + ApiUrls.DeleteTender + tenderId;
            var token = HttpContext.Session.GetString("JWToken");
            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var response = await client.DeleteAsync(url);

            var responseStream = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var responseData = JsonConvert.DeserializeObject<GenericResponse<IEnumerable<TenderDataForResponses>>>(responseStream);

                ViewBag.TenderListData = responseData.Data;
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {

                LoginMessage message = new LoginMessage
                {
                    Email = model.Email,
                    Password = model.Password,
                };

                var client = _httpClient.CreateClient();

                var url = ApiUrls.BaseServerUrl + ApiUrls.Login;
                var response = await client.PostAsJsonAsync(url, message);

                var responseStream = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {

                    var responseData = JsonConvert.DeserializeObject<LoginResponse>(responseStream);

                    HttpContext.Session.SetString("JWToken", responseData.Token);

                    //LogInfo($"The user with email:{model.Email} successfully logging in");

                    //        HttpContext.Session.SetString("UserId", responseDataAdminInfo.Data.UserId);
                    //        HttpContext.Session.SetString("Email", responseDataAdminInfo.Data.Email);
                    //        HttpContext.Session.SetString("Role", responseDataAdminInfo.Data.Role);

                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError(string.Empty, "Invalid login attempt.");

            }
            //LogWarning($"Invalid login attempt by email:{model.Email}");

            return View();
        }

        //public IActionResult Tender()
        //{
        //    //check login status
        //    if (HttpContext.Session.GetString("JWToken") == null)
        //    {
        //        return RedirectToAction("Login", "Home");
        //    }
        //    return View();
        //}


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task<(HttpResponseMessage, string)> FetchTender(TenderMessage message)
        {
            var client = _httpClient.CreateClient();

            var url = ApiUrls.BaseServerUrl + ApiUrls.Tender;
            var token = HttpContext.Session.GetString("JWToken");
            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var response = await client.PostAsJsonAsync(url, message);

            var responseStream = await response.Content.ReadAsStringAsync();
            return (response, responseStream);
        }

        private async Task FetchTenderData(string tenderId){
            TenderMessage message = new TenderMessage
            {
                Query = new TenderQuery()
                {
                    ID = Int64.Parse(tenderId)
                },
                Order = new OrderQuery
                {
                    OrderBy = "ID",
                    IsDescending = true
                },
                Limit = new LimitQuery
                {
                    StartCount = 0,
                    EndCount = 1
                }
            };
            var(response, responseStream) = await FetchTender(message);
            if (response.IsSuccessStatusCode)
            {
                var responseData = JsonConvert.DeserializeObject<GenericResponse<IEnumerable<TenderDataForResponses>>>(responseStream);
                ViewBag.TenderData = responseData.Data;
            }
        }

    }
}
