﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Messages
{
    public class LoginMessage
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
