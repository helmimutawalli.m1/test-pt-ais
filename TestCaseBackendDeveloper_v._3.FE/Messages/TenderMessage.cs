﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Messages
{
    public class TenderMessage
    {
        public TenderQuery Query { get; set; }
        public OrderQuery Order { get; set; }
        public LimitQuery Limit { get; set; }
    }

    public class TenderQuery
    {
        public long ID { get; set; }
        public long UserID { get; set; }
        public string Title { get; set; }
        public TimeBetweenQuery ReleaseDate { get; set; }
        public TimeBetweenQuery ClosingDate { get; set; }
    }
    public class TimeBetweenQuery
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }

    public class OrderQuery
    {
        public string OrderBy { get; set; }
        public bool IsDescending { get; set; }
    }

    public class LimitQuery
    {
        public int? StartCount { get; set; }
        public int? EndCount { get; set; }

        public static readonly int DefaultStartCount = 0;
        public static readonly int DefaultEndCount = 10;

        public static LimitQuery CreateAsDefault()
        {
            return new LimitQuery()
            {
                StartCount = DefaultStartCount,
                EndCount = DefaultEndCount
            };
        }

        public void FixAsDefault()
        {
            if (StartCount == null)
            {
                if (EndCount != null && EndCount - DefaultEndCount >= 0)
                {
                    StartCount = EndCount - DefaultEndCount;
                }
                else
                {
                    StartCount = DefaultStartCount;
                }
            }

            if (EndCount == null)
            {
                if (StartCount != null)
                {
                    EndCount = StartCount + DefaultEndCount;
                }
                else
                {
                    EndCount = DefaultEndCount;
                }
            }
        }

        public bool IsOnlyOneAndFirst()
        {
            return StartCount == 0 && EndCount == 1;
        }
    }
}
