﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Models
{
    public class TenderModel : BaseModel
    {
        public long UserID { get; set; }
        public UserModel UserModel { get; set; }

        public string Title { get; set; }
        public string Reference { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
    }
}
