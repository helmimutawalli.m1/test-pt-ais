﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.Utils
{
    public class ApiUrls
    {
        public static string BaseServerUrl { get; private set; }

        public static void SetBaseUrl(string url)
        {
            BaseServerUrl = url;
        }

        //Login
        public const string Login = "Auth/Login";

        //Tender
        public const string Tender = "Tender/Read";
        public const string AddTender = "Tender/Create";
        public const string UpdateTender = "Tender/Edit";
        public const string DeleteTender = "Tender/Delete?tenderId=";
    }
}
