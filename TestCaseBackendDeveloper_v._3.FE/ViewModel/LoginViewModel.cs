﻿using System.ComponentModel.DataAnnotations;

namespace TestCaseBackendDeveloper_v._3.FE.ViewModel
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(8)]
        public string Password { get; set; }
    }
}
