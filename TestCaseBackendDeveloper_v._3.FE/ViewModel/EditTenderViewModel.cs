﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCaseBackendDeveloper_v._3.FE.ViewModel
{
    public class EditTenderViewModel
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string Reference { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ClosingDate { get; set; }
        public string Description { get; set; }
    }
}
